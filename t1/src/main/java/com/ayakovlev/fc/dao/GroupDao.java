package com.ayakovlev.fc.dao;

import java.util.Set;

import com.ayakovlev.fc.bean.Group;

public interface GroupDao {
	void add (Group group);
	Set<Group> listGroups();
}
