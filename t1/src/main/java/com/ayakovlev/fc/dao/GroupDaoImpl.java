package com.ayakovlev.fc.dao;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ayakovlev.fc.bean.Group;

@Repository
public class GroupDaoImpl implements GroupDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(Group group) {
		sessionFactory.getCurrentSession().save(group);
	}

	@Override
	public Set<Group> listGroups() {
		@SuppressWarnings("unchecked")
		TypedQuery<Group> query = sessionFactory.getCurrentSession().createQuery("from Group g left join fetch g.teams");
		List<Group> list = query.getResultList();
		Set<Group> set = new TreeSet<Group>();
		set.addAll(list);
		return set;
	}

}
