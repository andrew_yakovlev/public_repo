package com.ayakovlev.fc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="FC_Game")
public class Game {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private Long teamId1;

	@Column
	private Long teamId2;

	@Column
	private Long goals1;

	@Column
	private Long goals2;
	
	@Column
	private String groupName;
	
	@Transient
	private String team1Name;
	
	@Transient
	private String team2Name;
	
	public Game() {	}

	public Game(Long teamId1, Long teamId2, Long goals1, Long goals2, String groupName) {
		super();
		this.teamId1 = teamId1;
		this.teamId2 = teamId2;
		this.goals1 = goals1;
		this.goals2 = goals2;
		this.groupName = groupName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTeamId1() {
		return teamId1;
	}

	public void setTeamId1(Long teamId1) {
		this.teamId1 = teamId1;
	}

	public Long getTeamId2() {
		return teamId2;
	}

	public void setTeamId2(Long teamId2) {
		this.teamId2 = teamId2;
	}

	public Long getGoals1() {
		return goals1;
	}

	public void setGoals1(Long goals1) {
		this.goals1 = goals1;
	}

	public Long getGoals2() {
		return goals2;
	}

	public void setGoals2(Long goals2) {
		this.goals2 = goals2;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getTeam1Name() {
		return team1Name;
	}

	public void setTeam1Name(String team1Name) {
		this.team1Name = team1Name;
	}

	public String getTeam2Name() {
		return team2Name;
	}

	public void setTeam2Name(String team2Name) {
		this.team2Name = team2Name;
	}

}
