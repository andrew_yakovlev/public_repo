package com.ayakovlev.fc.bean;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FC_Group")
public class Group implements Comparable<Group> {

	@Id
	private String name;

	public Group() {
	}

	public Group(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	Set<Team> teams = new HashSet<Team>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Team> getTeams() {
		return teams;
	}

	public void setTeams(Set<Team> teams) {
		this.teams = teams;
	}

	@Override
	public String toString() {
		return "Group [name=" + name + ", teams: " + teams.size() + "]";
	}

	@Override
	public int hashCode() {
		if (name == null) {
			return 0;
		}
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(!(obj instanceof Group)) {
			return false;
		}
		Group group = (Group)obj;
		if(name == null && group.name != null) {
			return false;
		}
		return name.equals(group.name);
	}

	@Override
	public int compareTo(Group o) {
		if (o == null || o.getName() == null) {
			return -1;
		} else {
			return -1 * o.getName().compareTo(name);
		}
	}

}
