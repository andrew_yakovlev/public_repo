package com.ayakovlev.fc.service;

import java.util.List;
import java.util.Set;

import com.ayakovlev.fc.bean.AnalyticBean;
import com.ayakovlev.fc.bean.Game;
import com.ayakovlev.fc.bean.Team;

public interface GameService {

	Game get(Long long1, Long long2);

	void save(Game game);

	List<Game> getGames();

	Set<AnalyticBean> getAnalytics(List<Game> games, List<Team> teams);

}
