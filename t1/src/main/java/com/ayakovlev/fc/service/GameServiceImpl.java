package com.ayakovlev.fc.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ayakovlev.fc.bean.AnalyticBean;
import com.ayakovlev.fc.bean.Game;
import com.ayakovlev.fc.bean.Team;
import com.ayakovlev.fc.dao.GameDao;
import com.ayakovlev.fc.dao.TeamDao;

@Service()
public class GameServiceImpl implements GameService {

	@Autowired
	private GameDao gameDao;

	@Autowired
	private TeamDao teamDao;

	@Transactional(readOnly = true)
	@Override
	public Game get(Long teamId1, Long teamId2) {
		return gameDao.get(teamId1, teamId2);
	}

	@Transactional
	@Override
	public void save(Game game) {
		gameDao.save(game);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Game> getGames() {
		List<Game> games = gameDao.getGames();
		games.forEach((game) -> {
			game.setTeam1Name(teamDao.get(game.getTeamId1()).getName());
			game.setTeam2Name(teamDao.get(game.getTeamId2()).getName());
		});
		return games;
	}

	@Override
	public Set<AnalyticBean> getAnalytics(List<Game> games, List<Team> teams) {
		Set<AnalyticBean> set = new TreeSet<AnalyticBean>(new Comparator<AnalyticBean>() {
			@Override
			public int compare(AnalyticBean o1, AnalyticBean o2) {
				if (o1.getScores() < o2.getScores()) {
					return 1;
				} else if (o1.getScores() > o2.getScores()) {
					return -1;
				} else {
					return o1.getTeam().getName().compareTo(o2.getTeam().getName());
				}
			}
		});

		if (games != null && teams != null && games.size() > 0 && teams.size() > 0) {
			Map<Long, AnalyticBean> map = new HashMap<Long, AnalyticBean>();
			teams.forEach(team -> {
				AnalyticBean bean = new AnalyticBean();
				bean.setTeam(team);
				map.put(team.getId(), bean);
			});

			games.forEach(game -> {
				Long teamId1 = game.getTeamId1();
				Long teamId2 = game.getTeamId2();
				Long goals1 = game.getGoals1();
				Long goals2 = game.getGoals2();
				AnalyticBean bean1 = map.get(teamId1);
				AnalyticBean bean2 = map.get(teamId2);
//				bean1.setTeam(teamDao.get(teamId1));
				if (goals1 < goals2) {
					bean1.setDefeat(bean1.getDefeat() + 1);
					bean2.setWins(bean2.getWins() + 1);
					bean1.setScores(bean1.getScores() + 0);
					bean2.setScores(bean2.getScores() + 3);
				} else if (goals1 > goals2) {
					bean1.setWins(bean1.getWins() + 1);
					bean2.setDefeat(bean2.getDefeat() + 1);
					bean1.setScores(bean1.getScores() + 3);
					bean2.setScores(bean2.getScores() + 0);
				} else {
					bean1.setDraw(bean1.getDraw() + 1);
					bean2.setDraw(bean2.getDraw() + 1);
					bean1.setScores(bean1.getScores() + 1);
					bean2.setScores(bean2.getScores() + 1);
				}
				bean1.setClogged(bean1.getClogged() + goals1);
				bean2.setClogged(bean2.getClogged() + goals2);
				bean1.setMissing(bean1.getMissing() + goals2);
				bean2.setMissing(bean2.getMissing() + goals1);
			});

			map.forEach((id, bean) -> {
				System.out.println(set.size() + ": before: " + set);
				set.add(bean);
				System.out.println(set.size() + ": after: " + set);
			});
		}
		return set;
	}

}
