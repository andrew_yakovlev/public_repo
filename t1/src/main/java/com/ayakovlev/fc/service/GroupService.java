package com.ayakovlev.fc.service;

import java.util.Set;

import com.ayakovlev.fc.bean.Group;

public interface GroupService {
	void add(Group group);
	Set<Group> listGroups();
}
