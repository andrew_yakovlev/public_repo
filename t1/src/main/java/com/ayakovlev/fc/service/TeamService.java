package com.ayakovlev.fc.service;

import java.util.List;

import com.ayakovlev.fc.bean.Team;

public interface TeamService {
	void add(Team team);
	List<Team> listTeams();
	List<Team> getUndistributedTeams();
	void distributeTeamsByGroups();
	Team get(Long teamId1);
}
