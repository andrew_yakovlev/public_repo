package com.ayakovlev.fc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ayakovlev.fc.bean.AnalyticBean;
import com.ayakovlev.fc.bean.Game;
import com.ayakovlev.fc.bean.Group;
import com.ayakovlev.fc.bean.Team;
import com.ayakovlev.fc.service.GameService;
import com.ayakovlev.fc.service.GroupService;
import com.ayakovlev.fc.service.TeamService;
import com.ayakovlev.fc.validate.Validator;

import java.util.List;
import java.util.Set;

@RestController
public class RequestController {

	private GroupService groupService;

	@Autowired
	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

	private TeamService teamService;

	@Autowired
	public void setTeamService(TeamService teamService) {
		this.teamService = teamService;
	}
	
	private GameService gameService;

	@Autowired
	public void setGameService(GameService gameService) {
		this.gameService = gameService;
	}
	
	@Autowired
	private Validator validator;
	
	@RequestMapping("makeGame")
	public ModelAndView makeGame(//
			@RequestParam("team1") String team1,//
			@RequestParam("team2") String team2,//
			@RequestParam("goals1") String goals1,//
			@RequestParam("goals2") String goals2//
			) {
		System.out.println("team1: " + team1);
		System.out.println("team2: " + team2);
		System.out.println("goals1: " + goals1);
		System.out.println("goals2: " + goals2);
		String error = validator.makeGame(team1, team2, goals1, goals2);
		if(StringUtils.isEmpty(error)) {
			Team tm1 = teamService.get(Long.parseLong(team1));
			// save game
			gameService.save(new Game(
					Long.parseLong(team1), 
					Long.parseLong(team2), 
					Long.parseLong(goals1), 
					Long.parseLong(goals2),
					tm1.getGroup().getName())
					);
		}
		ModelAndView model = welcomeScreen();
		model.addObject("error", error);
		return model;
	}
	
	@RequestMapping("/distributeTeams")
	public ModelAndView distributeTeams() {
        System.out.println("distributeTeams");
        teamService.distributeTeamsByGroups();
        ModelAndView model = welcomeScreen();
        return model;
	}

	@RequestMapping("/")
    public ModelAndView welcomeScreen() {
        ModelAndView model = new ModelAndView();
        model.setViewName("welcome");

		Set<Group> groups = groupService.listGroups();
		System.out.println("groups: " + groups);
		model.addObject("groups", groups);
		
		List<Team> teams = teamService.listTeams();
		System.out.println("teams: " + teams);
		model.addObject("teams", teams);
        
		List<Team> undistributedTeams = teamService.getUndistributedTeams();
		System.out.println("undistributedTeams: " + undistributedTeams);
		model.addObject("undistributedTeams", undistributedTeams);
		
		List<Game> games = gameService.getGames();
		model.addObject("games", games);
		
		Set<AnalyticBean> analyticBeans = gameService.getAnalytics(games, teams);
		model.addObject("analyticBeans", analyticBeans);
		
		return model;
    }
}