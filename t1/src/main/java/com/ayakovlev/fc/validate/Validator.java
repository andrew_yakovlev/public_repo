package com.ayakovlev.fc.validate;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ayakovlev.fc.bean.Game;
import com.ayakovlev.fc.bean.Team;
import com.ayakovlev.fc.service.GameService;
import com.ayakovlev.fc.service.TeamService;

@Component
public class Validator {
	
	private TeamService teamService;

	@Autowired
	public void setTeamService(TeamService teamService) {
		this.teamService = teamService;
	}
	
	private GameService gameService;

	@Autowired
	public void setGameService(GameService gameService) {
		this.gameService = gameService;
	}
	
	public String makeGame(String team1, String team2, String goals1, String goals2) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("Идентификатор первой команды должен быть целым числом.", team1);
		map.put("Идентификатор второй команды должен быть целым числом.", team2);
		map.put("Голы первой команды должны быть целым числом.", goals1);
		map.put("Голы второй команды должны быть целым числом.", goals2);
		for(String key : map.keySet()) {
			try {
				Long.parseLong(map.get(key));
			}catch(Exception ex) {
				return key;
			}
		}
		
		Long teamId1 = Long.parseLong(team1);
		Long teamId2 = Long.parseLong(team2);
		Team t1 = teamService.get(teamId1);
		Team t2 = teamService.get(teamId2);
		if(t1 == null) {
			return "Невозможно определить первую команду.";
		}
		if(t2 == null) {
			return "Невозможно определить вторую команду.";
		}
		if(t1.equals(t2)) {
			return "Надо выбрать разные команды.";
		}
		if(!t1.getGroup().equals(t2.getGroup())) {
			return "На групповом этапе могут играть команды только из одной группы.";
		}
		Game game = gameService.get(t1.getId(), t2.getId());
		if(game != null) {
			return "Результат этой игры уже введён.";
		}
		return null;
	}

}
